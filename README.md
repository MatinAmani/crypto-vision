# Crypto Vision

## A simple trading view application with randomly generated data and features like SMA and theme switching

### Screenshots of the application:

## ![Loading...](./screenshots/screenshot-1.png)

## ![Loading...](./screenshots/screenshot-2.png)

## ![Loading...](./screenshots/screenshot-3.png)

## ![Loading...](./screenshots/screenshot-light-theme.png)
