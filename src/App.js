import { Provider as ReduxProvider } from 'react-redux'
import { ThemeProvider } from '@material-ui/core/styles'
import { CssBaseline } from '@material-ui/core'
import store from './redux'
import { useSelector } from 'react-redux'

import Layout from './components/Layout'
import Main from './components/sections/Main'
import Theme, { selectTheme } from './theme'

const App = () => {
    return (
        <Layout>
            <Main />
        </Layout>
    )
}

const UIContainer = () => {
    const mode = useSelector(selectTheme)

    return (
        <ThemeProvider theme={Theme(mode)}>
            <CssBaseline />
            <App />
        </ThemeProvider>
    )
}

const ReduxContainer = () => (
    <ReduxProvider store={store}>
        <UIContainer />
    </ReduxProvider>
)

export default ReduxContainer
