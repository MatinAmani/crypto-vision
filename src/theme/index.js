import { createTheme } from '@material-ui/core/styles'

const Theme = (mode) =>
    createTheme({
        palette: {
            type: mode,
            chart: {
                dark: '#3dffff',
                light: '#367fba',
            },
        },
    })

export const selectTheme = (state) => state.theme
export default Theme
