export const GEN_PRICE = 'GEN_PRICE'
export const genPrice = (token, init, count, variation) => ({
    type: GEN_PRICE,
    payload: {
        token,
        init,
        count,
        variation,
    },
})

export const CLR_PRICES = 'CLR_PRICES'
export const clrPrices = () => ({
    type: CLR_PRICES,
})

export const CHANGE_TOKEN = 'CHANGE_TOKEN'
export const changeToken = (token) => ({
    type: CHANGE_TOKEN,
    payload: {
        token,
    },
})

export const RESET_PRICE = 'RESET_PRICE'
export const resetPrice = () => ({
    type: RESET_PRICE,
})
