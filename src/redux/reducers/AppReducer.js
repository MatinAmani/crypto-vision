import PriceGen from '../../components/helpers/PriceGen'
import { TOGGLE_THEME } from '../actions/UiActions'
import {
    GEN_PRICE,
    CLR_PRICES,
    CHANGE_TOKEN,
    RESET_PRICE,
} from '../actions/TokensActions'

const initialState = {
    theme: 'dark',
    tokens: {
        BTC: PriceGen(10000, 500, 0.08),
        ETH: PriceGen(5000, 500, 0.1),
        ADA: PriceGen(10, 500, 0.3),
        USDT: PriceGen(1, 500, 0.0005),
    },
    selectedToken: 'BTC',
}

const AppReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case TOGGLE_THEME:
            return {
                ...state,
                theme: state.theme === 'light' ? 'dark' : 'light',
            }

        case GEN_PRICE:
            return {
                ...state,
                tokens: {
                    ...state.tokens,
                    [payload.token]: PriceGen(
                        payload.init,
                        payload.count,
                        payload.variation
                    ),
                },
            }

        case CLR_PRICES:
            return {
                ...state,
                tokens: {},
            }

        case RESET_PRICE:
            return {
                ...state,
                tokens: {
                    BTC: PriceGen(10000, 500, 0.08),
                    ETH: PriceGen(5000, 500, 0.1),
                    ADA: PriceGen(10, 500, 0.3),
                    USDT: PriceGen(1, 500, 0.0005),
                },
            }

        case CHANGE_TOKEN:
            return {
                ...state,
                selectedToken: payload.token,
            }

        default:
            return state
    }
}

export default AppReducer
