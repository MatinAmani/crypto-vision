import { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Sample from '../charts/Sample'
import { useSelector } from 'react-redux'
import { selectToken } from '../../redux/selectors'
import { Switch } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
    container: {
        padding: theme.spacing(2),
    },
}))

const Main = () => {
    const classes = useStyles()
    const token = useSelector(selectToken)
    // const [showSMA, setSMA] = useState(true)
    const [interval, setInterval] = useState(15)
    var RawData = useSelector((state) => state.tokens[token])

    const data = RawData.map((p, index) => {
        let sma = undefined
        if (index >= interval - 1) {
            let sum = 0
            for (let i = index - interval + 1; i <= index; i++) {
                sum += RawData[i]
            }
            sma = sum / interval
        }

        return {
            arg: index,
            val: p,
            sma,
        }
    })

    // const handleSmaChange = () => setSMA(!showSMA)

    return (
        <div className={classes.container}>
            <Sample name={token} data={data} />
            {/* <Switch checked={showSMA} onChange={handleSmaChange} /> */}
        </div>
    )
}

export default Main
