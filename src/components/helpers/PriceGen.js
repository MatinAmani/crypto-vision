const PriceGen = (init, count, variation) => {
    const price = Array(count - 1)
    price[0] = init

    price.forEach((p, i) => {
        let candle = variation * (Math.random() * 2 - 1)
        price[i + 1] = p * (candle + 1)
    })

    return price
}

export default PriceGen
