import { useState } from 'react'
import {
    AppBar,
    Typography,
    Drawer,
    Toolbar,
    IconButton,
    Divider,
    List,
    ListItem,
    ListItemText,
    useMediaQuery,
    Button,
} from '@material-ui/core'
import Brightness2Icon from '@material-ui/icons/Brightness2'
import WbSunnyRoundedIcon from '@material-ui/icons/WbSunnyRounded'
import MenuRoundedIcon from '@material-ui/icons/MenuRounded'
import { useDispatch, useSelector } from 'react-redux'
import { toggleTheme } from '../../redux/actions/UiActions'
import { resetPrice } from '../../redux/actions/TokensActions'
import { changeToken } from '../../redux/actions/TokensActions'
import { makeStyles } from '@material-ui/core/styles'
import { drawerWidth, animationDuration } from '../constants'
import { selectTheme, selectTokens, selectToken } from '../../redux/selectors'
import { useTheme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
    appBar: {
        [theme.breakpoints.up('md')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
    },
    menuButton: {
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
        color: '#fff',
    },
    content: {
        [theme.breakpoints.up('md')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
    },
    drawer: {
        padding: theme.spacing(1, 0),
        width: drawerWidth,
    },
    title: {
        flexGrow: 1,
    },
    themeSwitcher: {
        margin: theme.spacing(0, 0, 0, 1),
    },
}))

const Layout = ({ children }) => {
    const theme = useTheme()
    const classes = useStyles()
    const dispatch = useDispatch()
    const mode = useSelector(selectTheme)
    const [open, setOpen] = useState(false)
    const isMobile = useMediaQuery(theme.breakpoints.down('sm'))

    const handleDrawerOpen = () => setOpen(true)
    const handleDrawerClose = () => setOpen(false)
    const handlePriceReset = () => dispatch(resetPrice())

    return (
        <div>
            <AppBar
                position="static"
                className={classes.appBar}
                color="primary"
            >
                <Toolbar>
                    <IconButton
                        className={classes.menuButton}
                        onClick={handleDrawerOpen}
                        edge="start"
                    >
                        <MenuRoundedIcon />
                    </IconButton>
                    <Typography className={classes.title} variant="h4">
                        Crypto Vision
                    </Typography>
                    <Button
                        variant="contained"
                        color="secondary"
                        onClick={handlePriceReset}
                    >
                        Re-Generate Prices
                    </Button>
                    <IconButton
                        className={classes.themeSwitcher}
                        edge="end"
                        onClick={() => dispatch(toggleTheme())}
                        color="inherit"
                    >
                        {mode === 'light' ? (
                            <Brightness2Icon />
                        ) : (
                            <WbSunnyRoundedIcon />
                        )}
                    </IconButton>
                </Toolbar>
            </AppBar>
            <MyDrawer
                open={open}
                handleClose={handleDrawerClose}
                isMobile={isMobile}
            />
            <div className={classes.content}>{children}</div>
        </div>
    )
}

const MyDrawer = ({ open, handleClose, isMobile }) => {
    const classes = useStyles()
    const dispatch = useDispatch()
    const tokens = Object.keys(useSelector(selectTokens))

    const [selectedToken, setSelectedToken] = useState(useSelector(selectToken))

    const handleTokenChange = (token) => {
        setSelectedToken(token)
        dispatch(changeToken(token))
        isMobile && handleClose()
    }

    return (
        <Drawer
            classes={{
                paper: classes.drawer,
            }}
            variant={isMobile ? 'temporary' : 'permanent'}
            anchor="left"
            open={open}
            onClose={handleClose}
            transitionDuration={animationDuration}
        >
            <Typography variant="h5" align="center" paragraph>
                Tokens
            </Typography>
            <Divider />
            <List>
                {tokens.map((token) => (
                    <ListItem
                        key={token}
                        button
                        selected={selectedToken === token}
                        onClick={() => {
                            handleTokenChange(token)
                        }}
                    >
                        <ListItemText>{token}</ListItemText>
                    </ListItem>
                ))}
            </List>
        </Drawer>
    )
}

export default Layout
