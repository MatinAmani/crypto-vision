import {
    Chart,
    ArgumentAxis,
    ValueAxis,
    LineSeries,
    ZoomAndPan,
    Title,
    Legend,
} from '@devexpress/dx-react-chart-material-ui'
import { Animation, ValueScale } from '@devexpress/dx-react-chart'
import { scaleLinear } from 'd3-scale'
import { useTheme } from '@material-ui/styles'
import { useSelector } from 'react-redux'
import { selectTheme } from '../../theme'

const Sample = ({ name, data }) => {
    const theme = useTheme()
    const mode = useSelector(selectTheme)
    const scale = () => scaleLinear()
    const modifyDomain = () => [0.9, 1.1]
    return (
        <>
            <Chart data={data}>
                <Title text={name} />
                {name === 'USDT' && (
                    <ValueScale factory={scale} modifyDomain={modifyDomain} />
                )}
                <ArgumentAxis />
                <ValueAxis />
                <LineSeries
                    name="Value"
                    argumentField="arg"
                    valueField="val"
                    color={
                        mode === 'light'
                            ? theme.palette.chart.light
                            : theme.palette.chart.dark
                    }
                />
                <LineSeries name="SMA" argumentField="arg" valueField="sma" />
                <Animation duration={1000} />
                <ZoomAndPan />
                <Legend position="top" />
            </Chart>
        </>
    )
}

export default Sample
